package ist.digitalplatform.gateway.filter;

import org.reactivestreams.Publisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferUtils;
import org.springframework.core.io.buffer.PooledDataBuffer;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpRequestDecorator;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.http.server.reactive.ServerHttpResponseDecorator;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

@Component
public class LogGatewayFilter implements GlobalFilter
        , Ordered
{
    private static Logger log = LoggerFactory.getLogger(LogGatewayFilter.class);

    private static final String LINE_SEPARATOR = "\n";


    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        // First we get here request in exchange
        ServerHttpRequestDecorator requestMutated = new ServerHttpRequestDecorator(exchange.getRequest()) {
            @Override
            public Flux<DataBuffer> getBody() {
                WriteLog requestLogger = new WriteLog(getURI(), getDelegate());
                return super
                        .getBody()
                        .map(ds -> {
                            requestLogger.appendBody(ds.asByteBuffer());
                            return ds;
                        })
                        .doFinally(s -> requestLogger.log());
            }
        };

        ServerHttpResponseDecorator responseMutated = new ServerHttpResponseDecorator(exchange.getResponse()) {
            @Override
            public Mono<Void> writeWith(Publisher<? extends DataBuffer> body) {
                WriteLog responseLogger = new WriteLog(getDelegate());
                return join(body)
                        .flatMap(ds -> {
                            responseLogger.appendBody(ds.asByteBuffer());
                            responseLogger.log();
                            return getDelegate().writeWith(Mono.just(ds));
                        });
            }
        };

        return chain.filter(exchange.mutate().request(requestMutated).response(responseMutated).build());
    }

    private Mono<? extends DataBuffer> join(Publisher<? extends DataBuffer> dataBuffers) {
        Assert.notNull(dataBuffers, "'dataBuffers' must not be null");
        return Flux
                .from(dataBuffers)
                .collectList()
                .filter(list -> !list.isEmpty())
                .map(list -> list.get(0).factory().join(list))
                .doOnDiscard(PooledDataBuffer.class, DataBufferUtils::release);
    }

    @Override
    public int getOrder() {
        return Ordered.HIGHEST_PRECEDENCE;
    }

    private static class WriteLog {

        private StringBuilder sb = new StringBuilder();

        WriteLog(ServerHttpResponse response) {
            sb.append(LINE_SEPARATOR);
            sb.append("---- Response -----").append(LINE_SEPARATOR);
            sb.append("Headers      \t\t:").append(response.getHeaders().toSingleValueMap()).append(LINE_SEPARATOR);
            sb.append("Status code  \t\t:").append(response.getStatusCode()).append(LINE_SEPARATOR);
        }

        WriteLog(URI url, ServerHttpRequest request) {
            sb.append(LINE_SEPARATOR);
            sb.append("---- Request ------").append(LINE_SEPARATOR);
            sb.append("URL          \t\t:").append(url).append(LINE_SEPARATOR);
            sb.append("Method       \t\t:").append(request.getMethod()).append(LINE_SEPARATOR);
            sb.append("Client       \t\t:").append(request.getRemoteAddress()).append(LINE_SEPARATOR);
            sb.append("Headers      \t\t:").append(request.getHeaders().toSingleValueMap()).append(LINE_SEPARATOR);
        }

        void appendBody(ByteBuffer byteBuffer) {
            sb.append("Body         \t\t:").append(StandardCharsets.UTF_8.decode(byteBuffer)).append(LINE_SEPARATOR);
        }

        void log() {
            sb.append("-------------------").append(LINE_SEPARATOR);
            log.info(sb.toString());
        }
    }
}
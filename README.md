# ist-gateway-sample

###API Employee

Original API
```
curl --location --request GET 'http://mf8.myinfosys.net:7006/castlemock/mock/rest/project/zBvdcr/application/4rxQCF/'
```
Gateway API
```
curl --location --request GET 'http://localhost:8080/employee/castlemock/mock/rest/project/zBvdcr/application/4rxQCF/'
```

###API Product
Original API
```
curl --location --request GET 'http://mf8.myinfosys.net:7006/castlemock/mock/rest/project/zBvdcr/application/bUj3XV/'
```

Gateway API
```
curl --location --request GET 'http://localhost:8080/product/castlemock/mock/rest/project/zBvdcr/application/bUj3XV/'
```